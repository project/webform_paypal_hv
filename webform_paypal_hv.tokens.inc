<?php

/**
 * Implements hook_token_info().
 */
function webform_paypal_hv_token_info() {
  $info['tokens']['submission']['paypal-hv-url'] = array(
    'name' => t('Webform PayPal (hv) url'),
    'description' => t('The PayPal payment plaform url.'),
  );

  return $info;
}

/**
 * Implements hook_tokens_alter().
 */
function webform_paypal_hv_tokens_alter(array &$replacements, array $context) {
  if (
    $context['type'] == 'submission'
    && !empty($context['tokens']['paypal-hv-url'])
    && !empty($context['data']['webform-submission'])
    ) {
    $replacements[$context['tokens']['paypal-hv-url']] = webform_paypal_hv_build_url($context['data']['webform-submission']);
  }
}
