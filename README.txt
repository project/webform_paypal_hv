This module provides PayPal button behaviors when submitting a
<a href="https://www.drupal.org/project/webform">Webform</a>. That is, it builds
an url upon Webform submission which redirect to the PayPal platform
(https://www.[sandbox.]paypal.com/cgi-bin/webscr).

That PayPal payment platform url is built by inserting pairs of variables and
values in the 'PayPal HTML variables' as lines in the input textarea of this
module Webform form settings:
<a href="https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/">HTML Variables for PayPal Payments Standard</a>

That 'PayPal HTML variables' input is processed through token_replace() and thus
can use the Webform submitted values to pass parameters to the PayPal payment
platform, see
<a href="https://www.drupal.org/node/2790291">Webform Submission Tokens</a>.

This module settings is set for each webform and is located in the fieldset
'PayPal settings' of 'Webform / Form settings' pages.
